Source: ghmm
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               d-shlibs,
               python3-dev,
               pkg-config,
               libxml2-dev,
               libgsl-dev,
               liblapacke-dev,
               zlib1g-dev,
               swig
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/ghmm
Vcs-Git: https://salsa.debian.org/med-team/ghmm.git
Homepage: https://sourceforge.net/p/ghmm/wiki/Home/
Rules-Requires-Root: no

Package: ghmm
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python3:Depends},
         python3,
         libghmm1
Recommends: libopenblas0 | libblis4
Description: General Hidden-Markov-Model library - tools
 The General Hidden Markov Model Library (GHMM) is a C library with
 additional Python3 bindings implementing a wide range of types of
 Hidden Markov Models and algorithms: discrete, continuous emissions,
 basic training, HMM clustering, HMM mixtures.
 .
 This package contains some tools using the library.

Package: libghmm-dev
Architecture: any
Section: libdevel
Depends: ${shlibs:Depends},
         ${misc:Depends},
         libghmm1 (>= ${source:Upstream-Version}),
         libghmm1 (<< ${source:Upstream-Version}+1)
Description: General Hidden-Markov-Model library - header files
 The General Hidden Markov Model Library (GHMM) is a C library with
 additional Python3 bindings implementing a wide range of types of
 Hidden Markov Models and algorithms: discrete, continuous emissions,
 basic training, HMM clustering, HMM mixtures.
 .
 Header files and static library to compile against the library.

Package: libghmm1
Architecture: any
Section: libs
Depends: ${shlibs:Depends},
         ${misc:Depends},
         python3
Description: General Hidden-Markov-Model library
 The General Hidden Markov Model Library (GHMM) is a C library with
 additional Python3 bindings implementing a wide range of types of
 Hidden Markov Models and algorithms: discrete, continuous emissions,
 basic training, HMM clustering, HMM mixtures.
 .
 The dynamic library.
