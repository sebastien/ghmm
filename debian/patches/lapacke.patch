Description: Use LAPACKE instead of ATLAS
Bug-Debian: https://bugs.debian.org/1056673
Author: Andreas Tille <tille@debian.org>
        Sébastien Villemot <sebastien@debian.org>
Last-Update: 2024-01-15
--- a/configure.in
+++ b/configure.in
@@ -202,69 +202,30 @@ AC_ARG_ENABLE(gsl_diagonal_hack,
 )
 
 
-dnl check lapack functions from atlas
-AC_ARG_ENABLE(atlas,
-  AC_HELP_STRING([--enable-atlas], [use matrix operations from atlas lapack [[default=no]]]),
+dnl check lapacke
+AC_ARG_ENABLE(lapacke,
+  AC_HELP_STRING([--enable-lapacke], [use matrix operations from lapacke [[default=no]]]),
   [
-    if test "x$enable_lapack" != "xno" ; then
+    if test "x$enable_lapacke" != "xno" ; then
 
-        AC_DEFINE(DO_WITH_ATLAS,1,[use matrix operations from lapack])
+        AC_DEFINE(DO_WITH_LAPACKE,1,[use matrix operations from lapacke])
 
-        echo -n "checking for lapack with pkg-config... "
-        if $PKGCONFIG --exists lapack; then
+        echo -n "checking for lapacke with pkg-config... "
+        if $PKGCONFIG --exists lapacke; then
                 AC_MSG_RESULT(yes)
-                LAPACK_CFLAGS=`$PKGCONFIG --cflags lapack`
-                LAPACK_LIBS=`$PKGCONFIG --libs lapack`
+                LAPACKE_CFLAGS=`$PKGCONFIG --cflags lapacke`
+                LAPACKE_LIBS=`$PKGCONFIG --libs lapacke`
         else
                 AC_MSG_RESULT(no)
-                AC_MSG_NOTICE(lapack not found with pkg-config)
-                AC_MSG_NOTICE(you have to add the necessary flags to CFLAGS)
+                AC_MSG_ERROR(lapacke not found with pkg-config)
         fi
 
-        LIBS="$LIBS $LAPACK_LIBS"
-        CFLAGS="$CFLAGS $LAPACK_CFLAGS"
-
-        dnl checking for clapack_dgetrf
-        AC_MSG_CHECKING([for clapack_dgetrf in -llapack])
-        AC_TRY_COMPILE([#include <clapack.h>],
-        [
-                double a;
-                int ipiv;
-                int x = clapack_dgetrf(0, 0, 0, &a, 0, &ipiv);
-        ],
-        AC_MSG_RESULT(yes)
-        AC_DEFINE(HAVE_CLAPACK_DGETRF,1,[clapack_dgetrf exists]),
-        AC_MSG_RESULT(no)
-        )
-
-        dnl checking for clapack_dgetri
-        AC_MSG_CHECKING([for clapack_dgetri in -llapack])
-        AC_TRY_COMPILE([#include <clapack.h>],
-        [
-                double a;
-                int ipiv;
-                int x = clapack_dgetri(0, 0, &a, 0, &ipiv);
-        ],
-        AC_MSG_RESULT(yes)
-        AC_DEFINE(HAVE_CLAPACK_DGETRI,1,[clapack_dgetri exists]),
-        AC_MSG_RESULT(no)
-        )
-
-        dnl checking for clapack_dpotrf
-        AC_MSG_CHECKING([for clapack_dpotrf in -llapack])
-        AC_TRY_COMPILE([#include <clapack.h>],
-        [
-                double a;
-                int x = clapack_dpotrf(0, 0, 0, &a, 0);
-        ],
-        AC_MSG_RESULT(yes)
-        AC_DEFINE(HAVE_CLAPACK_DPOTRF,1,[clapack_dpotrf exists]),
-        AC_MSG_RESULT(no)
-        )
+        LIBS="$LIBS $LAPACKE_LIBS"
+        CFLAGS="$CFLAGS $LAPACKE_CFLAGS"
     fi
   ],
   [
-    AC_MSG_NOTICE("not using lapack")
+    AC_MSG_NOTICE("not using lapacke")
   ]
 )
 
--- a/ghmm/matrixop.c
+++ b/ghmm/matrixop.c
@@ -44,8 +44,8 @@
 #include <gsl/gsl_linalg.h>
 #endif
 
-#ifdef DO_WITH_ATLAS
-#include <clapack.h>
+#ifdef DO_WITH_LAPACKE
+#include <lapacke.h>
 #endif
 
 /*============================================================================*/
@@ -87,7 +87,7 @@ int ighmm_invert_det(double *sigmainv, d
   }
 
   gsl_matrix_free(inv);
-#elif defined HAVE_CLAPACK_DGETRF && HAVE_CLAPACK_DGETRI
+#elif defined DO_WITH_LAPACKE
   char sign;
   int info, i;
   int *ipiv;
@@ -99,7 +99,7 @@ int ighmm_invert_det(double *sigmainv, d
   memcpy(sigmainv, cov, length * length * sizeof *cov);
   
   /* perform in-place LU factorization of covariance matrix */
-  info = clapack_dgetrf(CblasRowMajor, length, length, sigmainv, length, ipiv);
+  info = LAPACKE_dgetrf(LAPACK_ROW_MAJOR, length, length, sigmainv, length, ipiv);
   
   /* determinant */
   sign = 1;
@@ -112,7 +112,7 @@ int ighmm_invert_det(double *sigmainv, d
   *det = det_tmp * sign;
   
   /* use the LU factorization to get inverse */
-  info = clapack_dgetri(CblasRowMajor, length, sigmainv, length, ipiv);
+  info = LAPACKE_dgetri(LAPACK_ROW_MAJOR, length, sigmainv, length, ipiv);
   
   free(ipiv);
 #else
@@ -231,11 +231,11 @@ int ighmm_cholesky_decomposition (double
   }
   gsl_matrix_free(tmp);
 
-#elif defined HAVE_CLAPACK_DPOTRF
+#elif defined DO_WITH_LAPACKE
   int info;
   /* copy cov. matrix entries to result matrix, the rest is done in-place */
   memcpy(sigmacd, cov, dim * dim * sizeof *cov);
-  info = clapack_dpotrf(CblasRowMajor, CblasUpper, dim, sigmacd, dim);
+  info = LAPACKE_dpotrf(LAPACK_ROW_MAJOR, 'U', dim, sigmacd, dim);
 #else
   int row, j, k;
   double sum;
