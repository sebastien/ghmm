## Installation

`apt-get install libghmm-dev`

## GHMM test in python

Test in python can be referenced [here](https://ghmm.sourceforge.net/documentation.html)

## Executed command
`python3 ghmm_test.py`

- `ModuleNotFoundError: No module named 'ghmm'`


## GHMM test in C

Test is referenced from ghmm/tests/two_states_three_symbols.c


## Executed commands

- `gcc -c ghmm_test.c -o ghmm_test.o -I/usr/include/ghmm`
- `gcc ghmm_test.o -o ghmm_test -lghmm -llapack`
- `./ghmm_test`

#### Error
- `./ghmm_test: symbol lookup error: /lib/x86_64-linux-gnu/libghmm.so.1: undefined symbol: clapack_dgetrf`



