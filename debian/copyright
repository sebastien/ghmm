Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ghmm
Upstream-Contact: Alexander Schliep <alexander@schlieplab.org>
Source: https://sourceforge.net/projects/ghmm/

Files: *
Copyright:
         2001-2016 Alexander Schliep            <schliep@ghmm.org>
         2001-2016 Benjamin Georgi          <georgi@molgen.mpg.de>
         2001-2016 Wasinee Rungsarityotin <rungsari@molgen.mpg.de>
         2001-2016 Ivan G. Costa             <costa@molgen.mpg.de>
         2001-2016 Janne Grunau             <grunau@molgen.mpg.de>
         2001-2016 Matthias Heinig
Contributors:
         Ben Rich
         Utz Pape
         Andrea Weisse
         Joern Toedling
         Achim Goedke
         Bernd Wichern
         Bernhard Knab
         Peter Pipenbacher
         Thordis Linda Thorarinsdottir
         Alexander Riemer
         Anne-Katrin Emde
         Anyess von Bock
         Heval Benav
         Marie Hoffmann
         Marko Briesemann
         Markus Schueler
License: GPL-2.0+

Files: ghmm/ghmm.h
Copyright: 1998-2004 Alexander Schliep <schliep@ghmm.org>
           1998-2001 ZAIK/ZPR, Universitaet zu Koeln
           2002-2004 Max-Planck-Institut fuer Molekulare Genetik, Berlin
License: LGPL-2.0+
 On Debian systems you can find a full copy of the Library General Public
 License at /usr/share/common-licenses/LGPL-2.

Files: debian/*
Copyright: 2018 Steffen Moeller <moeller@debian.org>
License: GPL-2.0+

License: GPL-2.0+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
